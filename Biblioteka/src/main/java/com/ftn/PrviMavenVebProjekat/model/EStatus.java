package com.ftn.PrviMavenVebProjekat.model;

public enum EStatus {
    POSLAT, PRIHVACEN, POSLAT_NA_REVIZIJU, ODBIJEN
}
