package com.ftn.PrviMavenVebProjekat.service;


import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.Vest;


public interface VestService {
    Vest findOne(Long id);
    List<Vest> findAll();
    Vest save(Vest vest);
    Vest update(Vest vest);
    Vest delete(Long id);
}
