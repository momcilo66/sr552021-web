package com.ftn.PrviMavenVebProjekat.service;


import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.Nabavka;
/**SERVICE nabavka**/
public interface NabavkaService {
    Nabavka findOne(Long id);
    List<Nabavka> findAll();
    Nabavka save(Nabavka nabavka);
    Nabavka update(Nabavka nabavka);
    Nabavka delete(Long id);
}