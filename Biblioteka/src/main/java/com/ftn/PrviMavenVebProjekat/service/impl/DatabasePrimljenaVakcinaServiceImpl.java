package com.ftn.PrviMavenVebProjekat.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.PrviMavenVebProjekat.dao.PrimljenaVakcinaDAO;
import com.ftn.PrviMavenVebProjekat.model.PrimljenaVakcina;
import com.ftn.PrviMavenVebProjekat.service.PrimljenaVakcinaService;

@Service
public class DatabasePrimljenaVakcinaServiceImpl implements PrimljenaVakcinaService {

    @Autowired
    private PrimljenaVakcinaDAO primljenaVakcinaDAO;


    @Override
    public List<PrimljenaVakcina> findAll() {
        return primljenaVakcinaDAO.findAll();
    }
}