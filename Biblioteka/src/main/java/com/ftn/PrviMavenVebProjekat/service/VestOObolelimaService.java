package com.ftn.PrviMavenVebProjekat.service;

import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.VestOObolelima;


public interface VestOObolelimaService {
    VestOObolelima findOne(Long id);
    List<VestOObolelima> findAll();
    VestOObolelima save(VestOObolelima vestOObolelima);
    VestOObolelima update(VestOObolelima vestOObolelima);
    VestOObolelima delete(Long id);
}