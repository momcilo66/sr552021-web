package com.ftn.PrviMavenVebProjekat.service;


import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.PrimljenaVakcina;

public interface PrimljenaVakcinaService {
    List<PrimljenaVakcina> findAll();
}