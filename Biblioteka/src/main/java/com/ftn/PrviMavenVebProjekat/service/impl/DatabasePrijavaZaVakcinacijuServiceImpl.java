package com.ftn.PrviMavenVebProjekat.service.impl;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftn.PrviMavenVebProjekat.dao.PrijavaZaVakcinacijuDAO;
import com.ftn.PrviMavenVebProjekat.dao.VakcinaDAO;
import com.ftn.PrviMavenVebProjekat.model.Korisnik;
import com.ftn.PrviMavenVebProjekat.model.PrijavaZaVakcinaciju;
import com.ftn.PrviMavenVebProjekat.model.ProizvodjacVakcine;
import com.ftn.PrviMavenVebProjekat.model.Vakcina;
import com.ftn.PrviMavenVebProjekat.service.PrijavaZaVakcinacijuService;
import com.ftn.PrviMavenVebProjekat.service.VakcinaService;


@Service
public class DatabasePrijavaZaVakcinacijuServiceImpl implements PrijavaZaVakcinacijuService {

    @Autowired
    private PrijavaZaVakcinacijuDAO prijavaZaVakcinacijuDAO;



    @Override
    public PrijavaZaVakcinaciju findOne(Long id) {
        return prijavaZaVakcinacijuDAO.findOne(id);
    }

    @Override
    public List<PrijavaZaVakcinaciju> findAll() {
        return prijavaZaVakcinacijuDAO.findAll();
    }

    @Override
    public PrijavaZaVakcinaciju save(PrijavaZaVakcinaciju  prijavaZaVakcinaciju) {
        prijavaZaVakcinacijuDAO.save(prijavaZaVakcinaciju);
        return prijavaZaVakcinaciju;
    }



    @Override
    public PrijavaZaVakcinaciju delete(Long id) {
        PrijavaZaVakcinaciju prijavaZaVakcinaciju = prijavaZaVakcinacijuDAO.findOne(id);
        if(prijavaZaVakcinaciju != null) {
            prijavaZaVakcinacijuDAO.delete(id);
        }
        return prijavaZaVakcinaciju;
    }
    @Override
    public List<PrijavaZaVakcinaciju> find(String ime, String prezime, String jmbg) {

        return prijavaZaVakcinacijuDAO.find( ime,  prezime,  jmbg);

    }

}