package com.ftn.PrviMavenVebProjekat.dao;

import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.PrimljenaVakcina;

public interface PrimljenaVakcinaDAO {
    public List<PrimljenaVakcina> findAll();
}
