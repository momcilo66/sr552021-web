package com.ftn.PrviMavenVebProjekat.dao;


import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.Nabavka;

public interface NabavkaDAO {
    public Nabavka findOne(Long id);

    public List<Nabavka> findAll();


    public int save(Nabavka nabavka);

    public int update(Nabavka nabavka);

    public int delete(Long id);


}

