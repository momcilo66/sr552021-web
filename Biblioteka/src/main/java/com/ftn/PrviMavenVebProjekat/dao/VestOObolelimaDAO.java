package com.ftn.PrviMavenVebProjekat.dao;


import java.util.List;

import com.ftn.PrviMavenVebProjekat.model.VestOObolelima;

public interface VestOObolelimaDAO {

    public VestOObolelima findOne(Long id);

    public List<VestOObolelima> findAll();

    public int save(VestOObolelima vestOObolelima);

    public int update(VestOObolelima vestOObolelima);

    public int delete(Long id);
}